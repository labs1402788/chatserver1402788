import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import static java.lang.System.in;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class User implements Runnable {
    
    
    Scanner reader;
    History history;
    Socket userSocket;
    volatile boolean connected;
    String output;
    BufferedReader in;
    PrintWriter out;
    String nickName;

    public User(Socket s, History h){
        try{
        this.userSocket = s;
        this.history = h;
        this.connected = true;
        this.output = userSocket.getInetAddress().toString() + " connected.";
        in = new BufferedReader(new InputStreamReader(userSocket.getInputStream()));
        out = new PrintWriter(userSocket.getOutputStream(), true);
        //out.println("Type in your username: ");
        nickName = userSocket.getInetAddress().toString();
    }
        catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public synchronized void run(){
        while(this.connected){
        
            BufferedReader in = null;
        try {  
            in = new BufferedReader(new InputStreamReader(userSocket.getInputStream()));
            this.output = in.readLine();
            if (this.output == null || this.output.equals("/quit")){
                System.out.println(userSocket.getInetAddress() +": disconnected");
                this.connected = false;
                userSocket.close();
            }
            else{
            this.history.addHistory(this.output, nickName);
            }
            if(this.output.equals("/rename")){
                printMessage("Type a new username: ");
                this.nickName = in.readLine();
            }
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
}
    
    public String sendMessage (){
         
        return this.output;
}
    
    public void changeConnected(boolean s){
        this.connected = s;
    }
    public void printMessage(String s){
        out.println(s);
    }
    
    public boolean getConnected(){
        return this.connected;
    }
    public Socket getSocket(){
        return this.userSocket;
    }
}
