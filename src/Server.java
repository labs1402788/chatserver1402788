
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Server {
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        try {
            
            History history = new History();
            ServerSocket s1 = new ServerSocket(1027);
            ArrayList<Thread> threadList = new ArrayList<>();
            ArrayList<User> userList = new ArrayList<>();
            
            
                while(true){
                    try {
                        User newUser = new User(s1.accept(), history);
                        userList.add(newUser);
                        history.addUser(newUser);
                        Thread t1 = new Thread(newUser);
                        threadList.add(t1);
                        t1.start();
                        System.out.println("new user connected");
                        for(int i = 0; i<history.getHistory().size(); i++){
                            newUser.printMessage(history.getHistory().get(i));
                        }
                        System.out.println(newUser.getSocket().getInetAddress());
            } catch (IOException ex) {
                Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        }
            catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
            }
            
         
}
    

