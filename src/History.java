
import java.util.ArrayList;
import java.util.Date;


public class History {
    ArrayList<String> history;
    ArrayList<User> users;
    Date date;
    
    public History(){
        history = new ArrayList<>();
        users = new ArrayList<>();
        history.add("Welcome to the Gonacave!");
    }
    public void addUser(User u){
        this.users.add(u);
    }
    
    public void addHistory(String s, String nickname){
        date = new Date();
        this.history.add("<" + date.toString() + ">" + " [" + nickname + "]" + ": " + s + "\r");
        for(int i = 0;i < users.size();i++){
            this.users.get(i).printMessage("<" + date.toString() + ">" + " [" + nickname + "]" + ": " + s + "\r");
            System.out.println(date.toString() + ": " +s);
        }
        
    }
    
    public ArrayList<String> getHistory(){
        return history;
    }
    public String latestHistory(){
        return history.get(history.size()-1);
    }
    
}
